from django.contrib.auth.hashers import make_password, check_password
from django.shortcuts import render

from login import models


# Create your views here.
def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    if request.method == 'POST':
        username = request.POST.get('username')
        pwd = request.POST.get('password')
        password = make_password(pwd)

        res = models.User.objects.filter(username=username)
        if res:
            judge = check_password(pwd, password)
            return render(request, 'msg.html', {'msg': judge})

        models.User.objects.create(username=username, password=password)

        print(password)

        return render(request, 'msg.html', {'msg': password})