from django.db import models


# Create your models here.
class User(models.Model):
    username = models.CharField(verbose_name='用户名', max_length=155, primary_key=True)
    password = models.CharField(verbose_name='密码', max_length=155)
