import json
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from web import models


# Create your views here.
def test(request):
    if request.method == "GET":

        fields = models.Group.objects.values()
        fields = json.dumps(list(fields))
        print(list(fields))

        group = models.Group.objects.all()
        # res = serialize('json', group)
        # print(res)
        data = dict()
        for ob in group:
            data[ob.id] = ob.name
        data = json.dumps(data)
        return HttpResponse(data, content_type='application/json')
        # return render(request, 'test.html', {'res': group})
    if request.method == "POST":
        g = request.POST.get('group')
        return render(request, 'test.html', {'res': g})
