from django.urls import path, include

from web import views

urlpatterns = [
    path('test/', views.test, name='test')
]