package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * function:
 * author: shi
 * date: 2024/1/23 20:51
 */
@RestController
public class UserController {

    @Autowired
    private UserMapper userMapper;


    @GetMapping("/user")
    public List<User> query(){
        List<User> list = userMapper.find();
        return list;
    }

    @GetMapping("user/all")
    public List<User> queryAll(){
        return userMapper.selectAllUserAndOrders();
    }

}
