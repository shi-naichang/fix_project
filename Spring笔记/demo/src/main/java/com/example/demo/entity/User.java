package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * function:
 * author: shi
 * date: 2024/1/23 20:54
 */


@Data
@TableName("user")   // 对应数据库中的表名称
public class User {
    @TableId(type = IdType.AUTO, value = "id")  // 定义主键注解，类型是自增的
    private Integer id;
    @TableField(value = "username", exist = true)  // 对应数据库中的字段名称
    private String username;
    private String password;

    // 描述用户的订单
    @TableField(exist = false) // 表示这个字段在数据库中不存在，只作为对象的属性
    private List<Order> orders;
}
