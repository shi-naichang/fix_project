package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Order;
import com.example.demo.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {

//    @Select("select * from `order` where uid = #{uid}")
    public List<Order> selectByUid(Integer uid);

    // 查询订单所有的用户
//    @Select("select * from `order`")
//    @Results(
//            {
//                    @Result(property = "oid", column = "oid"),
//                    @Result(property = "uid", column = "uid"),
//                    @Result(property = "name", column = "name"),
//                    @Result(property = "user", column = "uid", javaType = User.class,
//                        one = @One(select = "com.example.demo.mapper.UserMapper.selectById")
//                    )
//            }
//    )
    public List<Order> selectOrderAndUser();
}
