package com.example.demo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * function:
 * author: shi
 * date: 2024/1/21 17:26
 */

@RestController
public class FileUploadController {

    @PostMapping("/upload")
    public String up(String nickname, MultipartFile file, HttpServletRequest request) {
        System.out.println("nickname = " + nickname);
        // 获取图片原始名称
        System.out.println(file.getOriginalFilename());
        // 获取文件类型
        System.out.println(file.getContentType());
        System.out.println(System.getProperty("user.dir"));

        String path = request.getServletContext().getRealPath("/upload/");
        System.out.println(path);
        return "success";
    }
}
