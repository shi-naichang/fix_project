package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select("select * from user")
    public List<User> find();


    // 查询所有的用户以及订单
//    @Select("select * from user")
//    @Results(
//            {
//                    @Result(column = "id", property = "id"),
//                    @Result(column = "username", property = "username"),
//                    @Result(column = "password", property = "password"),
//                    @Result(column = "id", property = "orders", javaType = List.class,
//                        many = @Many(select = "com.example.demo.mapper.OrderMapper.selectByUid")
//                    )
//            }
//    )
    public List<User> selectAllUserAndOrders();


    @Select("select * from user where id = #{id}")
    public User selectById(int id);
}
