package com.example.demo.controller;

import com.example.demo.entity.Order;
import com.example.demo.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * function:
 * author: shi
 * date: 2024/1/23 22:02
 */

@RestController
public class OrderController {
    @Autowired
    private OrderMapper orderMapper;


    @GetMapping("/order")
    public List<Order> query(){
        List<Order> list = orderMapper.selectByUid(1);
        return list;
    }


    @GetMapping("/order/all")
    public List<Order> queryAll(){
        List<Order> list = orderMapper.selectOrderAndUser();
        return list;
    }

    @GetMapping("/order/{id}")
    public List<Order> select(@PathVariable Integer id){
        List<Order> list = orderMapper.selectByUid(id);
        return list;
    }

}
