package com.example.demo.entity;

/**
 * function:
 * author: shi
 * date: 2024/1/23 21:29
 */

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName("order")
public class Order {
    @TableId(type = IdType.AUTO)
    private Long oid;
    private Long uid;
    private String name;

    @TableField(exist = false)
    private User user;
}
