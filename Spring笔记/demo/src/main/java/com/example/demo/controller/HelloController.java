package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * function:
 * author: shi
 * date: 2024/1/20 13:34
 */

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello() {
        return "Hello, World!!!!";
    }
}
