### 接口文档

+ 查看所有的维修项目，例如[水管漏水，暖气不暖]
  
  ```http
  /fix/item
  ```

+ 查看所有的维修区域，例如[一号楼，二号楼]
  
  ```http
  /fix/floor
  ```

+ 管理员登录接口
  
  ```http
  /admin/login
  ```

+ 维修工登录接口
  
  ```http
  /fixwork/login
  ```

+ 用户登录接口
  
  ```http
  /user/login
  ```

+ 用户注册接口
  
  ```http
  /user/register
  ```

+ 维修信息的插入
  
  ```http
  /fix/add
  ```

+ 查询全部维修信息
  
  ```http
  /fix/select
  ```

+ 根据openid查询全部维修信息
  
  ```http
  /fix/getinfo
  ```

+ 查询所有未维修的记录
  
  ```http
  /fix/unsuccess
  ```

+ 查询所有维修成功的记录
  
  ```http
  /fix/success
  ```

+ 根据id值修改维修状态，以及维修时间
  
  ```http
  /fix/update/{id}
  ```

### 数据库表

+ **admin表**  管理员表单
  
  | 属性       | 类型          | 注释            |
  |:--------:|:-----------:|:-------------:|
  | id       | bigint      | 成员id条目（主键，自增） |
  | phone    | varchar(11) | 手机号           |
  | username | varchar(64) | 用户名           |
  | password | varchar(64) | 密码            |

+ **fixwork表**  维修工表单
  
  | 属性       | 类型          | 注释          |
  |:--------:|:-----------:|:-----------:|
  | id       | bigint      | 维修条目（主键，自增） |
  | phone    | varchar(11) | 手机号         |
  | username | varchar(64) | 用户名         |
  | password | varchar(64) | 密码          |

+ **fixuser表**  用户信息表
  
  | 属性        | 类型           | 注释             |
  |:---------:|:------------:|:--------------:|
  | openid    | varchar(64)  | 微信小程序的唯一标识（主键） |
  | username  | varchar(30)  | 用户名            |
  | studentid | varchar(64)  | 学生的学号          |
  | password  | varchar(100) | 密码             |
  | role      | varchar(64)  | 用户的角色（student） |

+ **floorclass表**  维修区域的表单
  
  | 属性    | 类型          | 注释         |
  |:-----:|:-----------:|:----------:|
  | id    | int         | id值（主键，自增） |
  | floor | varchar(64) | 维修区域名称     |

+ **fixitem表**  维修项目的表单
  
  | 属性   | 类型          | 注释         |
  |:----:|:-----------:|:----------:|
  | id   | int         | id值（主键，自增） |
  | item | varchar(64) | 维修项目的名称    |

+ **subinfo表**  维修信息记录表单
  
  | 属性           | 类型           | 注释                        |
  |:------------:|:------------:|:-------------------------:|
  | openid       | varchar(64)  | 微信小程序的唯一标识（主键）            |
  | area         | varchar(100) | 维修区域名称                    |
  | areadetail   | varchar(255) | 详细维修区域                    |
  | fixpro       | varchar(100) | 维修项目的名称                   |
  | fixprodetail | varchar(255) | 详细维修项目描述                  |
  | photos       | varchar(255) | 维修图片名称                    |
  | name         | varchar(60)  | 提交者的姓名                    |
  | phone        | varchar(11)  | 提交者的电话                    |
  | subtime      | datetime     | 维修信息的提交时间                 |
  | status       | varchar(255) | 维修状态(未修：waiting；已修：fixed) |
  | updatetime   | datetime     | 维修信息的更新时间                 |
  | id           | bigint       | 维修条目（主键，自增）               |