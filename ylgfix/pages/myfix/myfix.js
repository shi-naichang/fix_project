// pages/myfix/myfix.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fixInfo:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

      wx.showLoading({
          title:"查询中",
      })
      wx.login({
        success: (res) => {
          var code = res.code
           wx.request({
          url: 'https://penghonghao.songciya.cn/ylgfixopenid',
          method:"POST",
          data:{
              code:code
          },success:(res_1)=>{
              wx.hideLoading()
              console.log("openid",res_1)
              this.setData({
                  openid:res_1.data.openid
              })
              this.myquery(res_1.data.openid)
          },fail:(res)=>{
              wx.hideLoading()
              console.error(res_1);
              wx.showToast({
                title: 'serverError',
                icon:"error"
              })
          }
      })
        },
      })
     
  },
  /* 查询 */
  myquery(openidtemp){
    console.log(openidtemp);
      wx.request({
        url: 'http://127.0.0.1:8555/fix/getinfo',
        method:"POST",
        data:{
            openid:openidtemp
        },success:(res)=>{
            console.log("查询成功",res);
            this.setData({
              fixInfo:res.data.data
            })
        },fail:(res)=>{
            console.error("查询报错",res);
        }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})