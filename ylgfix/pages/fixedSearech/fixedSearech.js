// pages/myfix/myfix.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fixInfo:[]
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
      wx.showLoading({
          title:"查询中",
      })
      wx.request({
          url: 'https://fixylg.xpwangluo.cn/fix/success',
          method:"GET",
          success:(res)=>{
              wx.hideLoading()
              console.log("查询成功",res);
              this.setData({
                fixInfo:res.data.data
              })
              console.log(this.data.fixInfo)
          },fail:(res)=>{
              wx.hideLoading()
              console.error("查询报错",res);
          }
      })
     
  },
  confirm(e){
    console.log(e.currentTarget.dataset.id);
    var id = e.currentTarget.dataset.id
    var index = e.currentTarget.dataset.index
    var tempArray=this.data.fixInfo
    tempArray.splice(index,1)
    this.setData({
      fixInfo:tempArray
    })
  }
})