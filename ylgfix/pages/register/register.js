// pages/login/login.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userName:"",
        userCode:"",
        openid:""
    },
    /* 跳转到注册页面 */
    NavToLogin(){
        wx.navigateTo({
          url: '/pages/login/login',
        })
    },
    /* 获取姓名 */
    collName(e){
        console.log(e.detail.value);
        this.setData({
            userName:e.detail.value
        })
    },
    /* 获取学号 */
    collCode(e){
        console.log(e.detail.value);
        this.setData({
            userCode:e.detail.value
        })
    },
    /* 获取openid */
    collOPenid(){
        wx.showLoading({
            title:"注册中"
        })
        wx.login({
          success: (res) => {
            console.log(res);
            var code = res.code
            wx.request({
              url: 'https://penghonghao.songciya.cn/ylgfixopenid',
              method:"POST",
              data:{
                  code:code
              },success:(res_1)=>{
                  console.log(res_1);
                  wx.hideLoading()
                  if(res_1.data.status=="ok"){
                      this.setData({
                          openid:res_1.data.openid
                      })
                      wx.request({
                        url: 'http://127.0.0.1:8555/user/login',
                        method:"POST",
                        data:{
                            studentid:this.data.userCode,
                            username:this.data.userName,
                            openid:this.data.openid
                        },success:(res_2)=>{
                            console.log(res_2);
                        },fail:(res)=>{
                            console.error(res);
                        }
                      })
                  }else{
                      wx.showToast({
                        title: '身份错误',
                        icon:"none"
                      })
                  }
              },fail:(res)=>{
                  wx.hideLoading()
                  console.error(res);
              }
            })
          },fail:(res)=>{
              wx.showToast({
                title: '授权错误',
              })
          }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },
})