// pages/myfix/myfix.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fixInfo:[]
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
      wx.showLoading({
          title:"查询中",
      })
      wx.request({
          url: 'http://127.0.0.1:8555/fix/unsuccess',
          method:"GET",
          success:(res)=>{
              wx.hideLoading()
              console.log("查询成功",res);
              this.setData({
                fixInfo:res.data.data
              })
          },fail:(res)=>{
              wx.hideLoading()
              console.error("查询报错",res);
          }
      })
     
  },
  confirm(e){
    console.log(e.currentTarget.dataset.id);
    var id = e.currentTarget.dataset.id
    var index = e.currentTarget.dataset.index
    wx.showModal({
      title: '确定已完成报修？',
      content: '维修人员或管理员操作',
      complete: (res) => {
        if (res.cancel) {
          wx.showToast({
            title: '您取消维修确定',
            icon:"none"
          })
        }
        if (res.confirm) {
          wx.request({
            url: 'http://127.0.0.1:8555/fix/update/'+id,
            method:"PUT",
            success:(res_1)=>{
                console.log(res_1);
                if(res_1.data.code=="200"){
                    wx.showToast({
                      title: '维修成功',
                    })
                    var tempArray=this.data.fixInfo
                    tempArray.splice(index,1)
                    this.setData({
                      fixInfo:tempArray
                    })
                }else{
                  wx.showToast({
                    title: '未成功请重试',
                    icon:"none"
                  })
                }
            },fail:(res_1)=>{
              console.error(res_1);
            }
        })
        }
      }
    })
  }
})