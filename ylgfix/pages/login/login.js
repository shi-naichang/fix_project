// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      username:"",
      openid:"",
      userpassword:"",
      roleClass:['学生','维修工','管理员'],
      selectRole:"请选择身份",
      userInputPla:"请输入学号"
  },
  /* 跳转到注册页面 */
  NavToRegister(){
      wx.navigateTo({
        url: '/pages/register/register',
      })
  },
  /* 获取账号 */
  userInput(e){
      console.log(e.detail.value);
      this.setData({
          username:e.detail.value
      })
  },
  /* 获取密码 */
  pwdInput(e){
      console.log(e.detail.value);
      this.setData({
          userpassword:e.detail.value
      })
  },
  /* 选择身份 */
  bindPickerChange(e){
      /* console.log(e); */
      var item = this.data.roleClass[e.detail.value]
      console.log(item);
      this.setData({
          selectRole:item
      })
      if(item!="学生"){
          this.setData({
              userInputPla:"请输入手机号"
          })
      }else{
          this.setData({
              userInputPla:"请输入学号"
          })
      }
  },
  /* 获取openid */
  login(){
      if(this.data.username.length==0 || this.data.userpassword.length==0){
          wx.showToast({
            title: '填写完整',
            icon:"error"
          })
          return
      }
      if(this.data.selectRole=="请选择身份" || this.data.selectRole.length==0){
          wx.showToast({
              title: '请选择身份',
              icon:"error"
            })
            return
      }
      if(this.data.selectRole=="学生"){
          console.log("student")
          wx.login({
              success: (res) => {
                console.log(res.code)
                var code = res.code
                if(res.code){
                    wx.request({
                        url: 'https://penghonghao.songciya.cn/ylgfixopenid',
                        method:"POST",
                        data:{
                            code:code
                        },success:(res_1)=>{
                            console.log("openid",res_1)
                            this.setData({
                                openid:res_1.data.openid
                            })
                            this.studentloginConfirm()
                        },fail:(res)=>{
                            console.error(res_1);
                            wx.showToast({
                              title: 'serverError',
                              icon:"error"
                            })
                        }
                    })
                }else{
                    wx.showToast({
                      title: '刷新重试',
                      icon:"error"
                    })
                }
              },
            })
      }if(this.data.selectRole=="维修工"){
          this.fixLogin()
      }if(this.data.selectRole=="管理员"){
          this.adminLogin()
      }
  },
  /* 学生登录 */
  studentloginConfirm(){
      wx.request({
        url: 'http://127.0.0.1:8555/user/login',
        method:"POST",
        data:{
            studentid:this.data.username,
            password:this.data.userpassword,
            openid:this.data.openid,
        },success:(res_2)=>{
              console.log(res_2)
        },fail:(res_2)=>{
              console.error(res_2)
        }
      })
  },
  /* 维修工登录 */
  fixLogin(){
      wx.request({
          url: 'http://127.0.0.1:8555/fixwork/login',
          method:"POST",
          data:{
              phone:this.data.username,
              password:this.data.userpassword,
          },success:(res_2)=>{
              console.log(res_2)
          },fail:(res_2)=>{
              console.error(res_2)
          }
      })
  },
  /* 管理员登录 */
  adminLogin(){
      wx.request({
          url: 'http://127.0.0.1:8555/admin/login',
          method:"POST",
          data:{
              phone:this.data.username,
              password:this.data.userpassword,
          },success:(res_2)=>{
              console.log(res_2)
          },fail:(res_2)=>{
              console.error(res_2)
          }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
})