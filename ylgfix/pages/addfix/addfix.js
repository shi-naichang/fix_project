// pages/addfix/addfix.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      area:["服务不可用"],
      areaSelected:"请选择区域",
      fixProject:["服务不可用"],
      fixProjectSelect:"请选择项目",
      areadetail:"",
      introduceDetail:"",
      name:"",
      phone:"",
      openid:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
      //请求区域
      wx.showLoading({
          title:"初始化中"
      })
      //areaClass
      wx.request({
        url: 'https://fixylg.xpwangluo.cn/fix/floor',
        method:"GET",
        success:(res)=>{
            wx.hideLoading()
            console.log(res);
            var arrayTemp=[]
            for(var i=0;i<res.data.data.length;i++){
                arrayTemp.push(res.data.data[i].floor)
                this.setData({
                  area:arrayTemp
                })
            }
        },fail:(res)=>{
            wx.hideLoading()
            wx.showToast({
              title: 'serverError',
              icon:"error"
            })
            console.error(res);
        }
      })
      //请求详细描述
      wx.request({
          url: 'https://fixylg.xpwangluo.cn/fix/item',
          method:"GET",
          success:(res)=>{
              wx.hideLoading()
              console.log(res);
              var arrayTemp=[]
              for(var i=0;i<res.data.data.length;i++){
                  arrayTemp.push(res.data.data[i].item)
                  this.setData({
                    fixProject:arrayTemp
                  })
              }
          },fail:(res)=>{
              wx.hideLoading()
              wx.showToast({
                title: 'serverError',
                icon:"error"
              })
              console.error(res);
          }
        })
        wx.login({
          success: (res) => {
            var code = res.code
            wx.request({
              url: 'https://penghonghao.songciya.cn/ylgfixopenid',
              method:"POST",
              data:{
                  code:code
              },success:(res_1)=>{
                  console.log("openid",res_1)
                  this.setData({
                      openid:res_1.data.openid
                  })
              },fail:(res)=>{
                  console.error(res_1);
                  wx.showToast({
                    title: 'serverError',
                    icon:"error"
                  })
              }
          })
          },fail:(res)=>{
              console.error(res);
          }
        })
  },
  /* 报修区域 */
  bindPickerChange(e){
      console.log(e.detail.value);
      var item = this.data.area[e.detail.value]
      console.log(item);
      this.setData({
          areaSelected:item
      })
  },
  /* 选择维修项目 */
  bindPickerChangePrj(e){
      console.log(e.detail.value);
      var item = this.data.fixProject[e.detail.value]
      console.log(item);
      this.setData({
          fixProjectSelect:item
      })
  },
  /* 报修区域 */
  areaDetailInput(e){
      console.log(e.detail.value);
      this.setData({
          areadetail:e.detail.value
      })
  },
   /* 描述详细 */
   introduceDetailInput(e){
      console.log(e.detail.value);
      this.setData({
          introduceDetail:e.detail.value
      })
  },
  /* 姓名 */
  nameInput(e){
      console.log(e.detail.value);
      this.setData({
          name:e.detail.value
      })
  },
  /* 手机号 */
  phoneInput(e){
      console.log(e.detail.value);
      this.setData({
          phone:e.detail.value
      })
  },
  /* 选择照片 */
  bindchoose(){
      wx.chooseMedia({
          count:1,
          sourceType: ['album', 'camera'],
          camera: 'back',
          success:(res)=>{
              console.log(res);
              const tempFilePath = res.tempFiles[0].tempFilePath
              console.log(tempFilePath);
              wx.showLoading({
                title: '加载中',
              })
              wx.uploadFile({
                filePath: tempFilePath,
                name: 'img',
                url: 'https://fixylg.xpwangluo.cn/submitting',
                success:(res_1)=>{
                  wx.hideLoading()
                  console.log(res_1);
                },fail:(res_1)=>{
                  wx.hideLoading()
                  console.error(res_1);
                }
              })
          } 
      })
  },
  /* submit */
  submit(){
      var pattern = /^1[3-9][0-9]{9}$/
      if(this.data.areaSelected=="请选择区域"){
          wx.showToast({
            title: '请选择区域',
            icon:"none"
          })
          return
      }if(this.data.fixProjectSelect=="请选择项目"){
          wx.showToast({
              title: '请选择项目',
              icon:"none"
            })
            return
      }if(this.data.areadetail.length==0 || this.data.introduceDetail.length==0 || this.data.name.length==0 || this.data.phone.length==0){
          wx.showToast({
            title: '请填写完整',
            icon:"none"
          })
          return
      }if(!pattern.test(this.data.phone)){
          wx.showToast({
            title: '手机号格式错误',
            icon:"none"
          })
          return
      }
      wx.request({
        url: 'https://fixylg.xpwangluo.cn/fix/add',
        method:"POST",
        data:{
            area:this.data.areaSelected,
            areadetail:this.data.areadetail,
            fixpro:this.data.fixProjectSelect,
            fixprodetail:this.data.introduceDetail,
            name:this.data.name,
            phone:this.data.phone,
            openid:this.data.openid,
            photos:"https://img0.baidu.com/it/u=757258298,486014382&fm=253&fmt=auto&app=138&f=JPEG?w=600&h=450"
        },success:(res)=>{
            console.log(res);
        },fail:(res)=>{
            console.error(res);
        }
      })
  }
})